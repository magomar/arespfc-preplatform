package ares.gui;

import ares.engine.Clock;
import ares.model.forces.Asset;
import ares.model.forces.Force;
import ares.model.forces.Formation;
import ares.model.forces.Unit;
import ares.model.scenario.Scenario;
import ares.model.scenario.ScenarioInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class InfoPanel extends javax.swing.JPanel {

    /**
     * Creates new form InfoPanel
     */
    public InfoPanel() {
        initComponents();
    }

    void showUnitInfo(Unit unit) {
        StringBuilder sb = new StringBuilder("=== UNIT ===\n");
        sb.append("Name: ").append(unit.getName()).append('\n');
        sb.append("Type: ").append(unit.getType()).append('\n');
        sb.append("Formation: ").append(unit.getFormation()).append('\n');
        sb.append("Force: ").append(unit.getForce()).append('\n');
        sb.append("Echelon: ").append(unit.getEchelon()).append('\n');
        sb.append("Movement Type: ").append(unit.getMovement()).append('\n');
        sb.append("\n=== STATUS ===\n");
        sb.append("Location: ").append(unit.getLocation()).append('\n');
        sb.append("Op. State: ").append(unit.getOpState()).append('\n');
        sb.append("Emphasis: ").append(unit.getEmphasis()).append('\n');
        sb.append("Stamina").append(unit.getStamina()).append('\n');
        sb.append("Speed: ").append(unit.getSpeed()).append('\n');
        sb.append("Range: ").append(unit.getRange()).append('\n');
        sb.append("Readiness: ").append(unit.getReadiness()).append('\n');
        sb.append("Supply: ").append(unit.getSupply()).append('\n');
        sb.append("\n=== STRENGTHS: ===\n");
        sb.append("AntiPersonnel: ").append(unit.getAntiPersonnel()).append('\n');
        sb.append("AntiTank: ").append(unit.getAntiTank()).append('\n');
        sb.append("Defense: ").append(unit.getDefense()).append('\n');
        sb.append("AntiAir (Low): ").append(unit.getLowAntiAir()).append('\n');
        sb.append("AntiAir (High): ").append(unit.getHighAntiAir()).append('\n');
        sb.append("Reconossaince: ").append(unit.getReconnaissance()).append('\n');
        sb.append("Artillery: ").append(unit.getArtillery()).append('\n');
        sb.append("Artillery range: ").append(unit.getArtilleryRange()).append('\n');
        sb.append("\n=== ASSETS: ===\n");
        for (Asset asset : unit.getAssets()) {
            sb.append(asset.getType().getName()).append(": ").append(asset.getNumber()).append('/').append(asset.getMax()).append('\n');
        }
        unitInfoArea.setText(sb.toString());
    }

    void showScenarioInfo(Scenario scenario) {
        ScenarioInfo info = scenario.getGameInfo();
        scenarioName.setText(info.getName());
        beginDate.setText("Begins: " + Clock.DATE_FORMAT.format(info.getBegins().getTime()));
        endDate.setText("Ends: " + Clock.DATE_FORMAT.format(info.getEnds().getTime()));
    }

    void showClockInfo(Scenario scenario) {
        Clock clock = scenario.getClock();
        date.setText("Time: " + Clock.DATE_FORMAT.format(clock.getNow().getTime()));
    }

    /*
     * Fills the OOBTree with the current active units
     */
    void populateOOBTree(Scenario scenario) {

        // map<forceName, forceNode>
        Map<String, DefaultMutableTreeNode> formationNodesMap = new HashMap<>();
        Map<String, DefaultMutableTreeNode> forceNodesMap = new HashMap<>();

        // We get the active units from clock
        Clock clock = scenario.getClock();
        List<Unit> unitList = clock.getUnits();                 // Current units
        List<Formation> formationList = clock.getFormations();  // Current formations
        Force[] forceArray = scenario.getForces();              // Forces

        // OOB Tree Root node
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Active Units");

        // Tree Model
        DefaultTreeModel oobTModel = new DefaultTreeModel(rootNode);

        // Inserts force nodes into oobTree
        for (int i = 0; i < forceArray.length; ++i) {

            String forceName = forceArray[i].getName();
            DefaultMutableTreeNode forceNode = new DefaultMutableTreeNode(forceName);

            forceNodesMap.put(forceName, forceNode);
            oobTModel.insertNodeInto(forceNode, rootNode, rootNode.getChildCount());
        }

        // Inserts formation nodes into the corresponding force node
        for (int i = 0; i < formationList.size(); ++i) {

            Formation form = formationList.get(i);
            String formName = form.getName();
            DefaultMutableTreeNode formNode = new DefaultMutableTreeNode(formName);

            formationNodesMap.put(formName, formNode);
            forceNodesMap.get(form.getForce().getName()).add(formNode);
        }

        for (Unit unit : unitList) {
            DefaultMutableTreeNode unitNode = new DefaultMutableTreeNode(unit.getName());
            unitNode.setUserObject(unit);
            formationNodesMap.get(unit.getFormation().getName()).add(unitNode);
        }

        oobTree.setModel(oobTModel);
        oobTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        oobTree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) oobTree.getLastSelectedPathComponent();

                /* if nothing is selected */
                if (node == null) {
                    return;
                }

                /* retrieve the node that was selected */
                Unit selectedUnit = (Unit)node.getUserObject();

                /* React to the node selection. */
                showUnitInfo(selectedUnit);
                // TODO
                //selectedUnit.setSelected(true);
            }
        });
    }

       /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scenarioPane = new javax.swing.JPanel();
        scenarioName = new javax.swing.JLabel();
        beginDate = new javax.swing.JLabel();
        endDate = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        infoTabbedPane = new javax.swing.JTabbedPane();
        unitScrollPane = new javax.swing.JScrollPane();
        unitInfoArea = new javax.swing.JTextArea();
        oobScrollPanel = new javax.swing.JScrollPane();
        oobTree = new javax.swing.JTree();

        scenarioName.setText("Scenario:");

        beginDate.setText("Begins:");

        endDate.setText("Ends:");

        date.setText("Time:");

        org.jdesktop.layout.GroupLayout scenarioPaneLayout = new org.jdesktop.layout.GroupLayout(scenarioPane);
        scenarioPane.setLayout(scenarioPaneLayout);
        scenarioPaneLayout.setHorizontalGroup(
            scenarioPaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scenarioPaneLayout.createSequentialGroup()
                .addContainerGap()
                .add(scenarioPaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(scenarioName, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                    .add(beginDate, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(endDate, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(date, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        scenarioPaneLayout.setVerticalGroup(
            scenarioPaneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scenarioPaneLayout.createSequentialGroup()
                .addContainerGap()
                .add(scenarioName)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(beginDate)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(endDate)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(date)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        unitInfoArea.setColumns(20);
        unitInfoArea.setRows(5);
        unitScrollPane.setViewportView(unitInfoArea);

        infoTabbedPane.addTab("Info", unitScrollPane);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Active Units");
        oobTree.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        oobScrollPanel.setViewportView(oobTree);

        infoTabbedPane.addTab("OOB", oobScrollPanel);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scenarioPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(infoTabbedPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(scenarioPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(infoTabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
        );

        infoTabbedPane.getAccessibleContext().setAccessibleDescription("");
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel beginDate;
    private javax.swing.JLabel date;
    private javax.swing.JLabel endDate;
    private javax.swing.JTabbedPane infoTabbedPane;
    private javax.swing.JScrollPane oobScrollPanel;
    private javax.swing.JTree oobTree;
    private javax.swing.JLabel scenarioName;
    private javax.swing.JPanel scenarioPane;
    private javax.swing.JTextArea unitInfoArea;
    private javax.swing.JScrollPane unitScrollPane;
    // End of variables declaration//GEN-END:variables
}
