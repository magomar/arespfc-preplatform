/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares.gui;

import ares.io.AresIO;
import ares.io.AresPaths;
import ares.model.board.Board;
import java.awt.image.BufferedImage;
import java.io.File;
import util.ImageUtils;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class BoardInfo {

    // TODO: Dynamically get the tiles folder name instead of using the static string "/Medium"
    final public String GRAPHICS_PATH = AresIO.ARES_IO.getAbsolutePath(AresPaths.GRAPHICS.getPath()+"/Medium").toString();
    final public int hexRadius;
    final public int hexDiameter;
    final public int hexHeight;
    final public int hexSide;
//    final public int borderSide;
//    final public int borderHeight;
//    final public int borderRadius;
    final public int width;
    final public int height;

    public BoardInfo(Board board) {
        width = board.getWidth();
        height = board.getHeight();
        hexDiameter = (int) getDiameter(ImageUtils.loadImage(new File(GRAPHICS_PATH, "tiles_misc.png")), 10);
        hexRadius = hexDiameter / 2;
        hexSide = hexRadius * 3 / 2;
        hexHeight = (int) (hexRadius * Math.sqrt(3));
        
//        borderRadius = (int) ImageUtils.getRadius(miscHexGraphic, 10);
//        borderHeight = (int) (borderRadius * Math.sqrt(3));
//        borderSide = borderRadius * 3 / 2;
    }
    
        /*
     * Devuelve el radio del hexágono de la imagen que le pasamos, hay que pasarle el número de columnas
     * de la imagen
     */
    
    private static double getDiameter(BufferedImage ima, int cols)  {
        return ima.getWidth() / cols;
    }

}
