package ares.platform;

import ares.engine.Clock;
import ares.gui.MessagesPanel;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class MessageLogger {

    private static MessagesPanel messagesPane;
    private static Clock clock;

    private MessageLogger() {
        
    }

    public static void init(MessagesPanel messagesPane, Clock clock) {
        MessageLogger.messagesPane = messagesPane;          
        MessageLogger.clock = clock;
        messagesPane.clear();
    }


    public static void info(String msg) {
         messagesPane.showMessage('[' + clock.toString() + "] " + msg + '\n');
    }
}
