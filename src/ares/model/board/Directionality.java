/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares.model.board;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum Directionality {

    LOGICAL, 
    GRAPHICAL, 
    NONE
}
