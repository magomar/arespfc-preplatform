/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares.model.board;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum TerrainMisc {
    OPEN,
    ANCHORAGE,
    AIRFIELD,
    PEAK,
    CONTAMINATED,
    NON_PLAYABLE,
    MUDDY,
    SNOWY,
    BRIDGE_DESTROYED,
    FROZEN,
    EXCLUDED_1,
    EXCLUDED_2;

}
