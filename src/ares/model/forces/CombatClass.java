package ares.model.forces;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum CombatClass {
    HQ,
    LINE,
    LINE_SUPPORT,
    SUPPORT,
    SERVICE_SUPPORT,
    OTHER,
}
