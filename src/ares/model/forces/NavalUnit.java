package ares.model.forces;

import ares.model.scenario.Scenario;
import ares.engine.movement.MovementType;

/**
 *
 * @author Mario Gomez <margomez antiTank dsic.upv.es>
 */
public class NavalUnit extends SurfaceUnit {

    public NavalUnit(ares.data.jaxb.Unit unit, Formation formation, Force force, Scenario scenario) {
        super(unit, formation, force, scenario);
        movement = MovementType.NAVAL;
        speed = Integer.MAX_VALUE;
        for (Asset asset : assets) {
            AssetType ast = asset.getType();
            int n = asset.getNumber();
            if (n > 0) {
                speed = Math.min(speed, ast.getSpeed());
            }
        }
    }
    
}
