package ares.model.forces;

import ares.data.jaxb.Emphasis;
import ares.data.jaxb.Formation.Track;
import ares.data.jaxb.Formation.Track.Objective;
import ares.data.jaxb.SupportScope;
import ares.model.scenario.Scenario;
import ares.engine.Clock;
import ares.engine.action.Action;
import ares.engine.action.ActionState;
import ares.engine.action.ActionType;
import ares.engine.action.ChangeDeploymentAction;
import ares.engine.action.SurfaceMoveAction;
import ares.engine.command.OperationType;
import ares.engine.movement.MovementType;
import ares.model.board.Direction;
import ares.model.board.Tile;
import java.util.*;
import util.RandomGenerator;

/**
 *
 * @author Mario Gomez <margomez antiTank dsic.upv.es>
 */
public class Formation {
    private Scenario scenario;
    private int id;
    private String name;
    private Echelon echelon;
    private Force force;
    private String commander;
    private String details;
    private int proficiency;
    private int supply;
    private OperationType orders;
    private Emphasis emphasis;
    private SupportScope supportscope;
    /**
     * List of objectives (used by the programmed opponent to generate plans)
     */
    private List<Tile> objectives;
//    private Collection<Unit> units;
    /**
     * Collection of available (on-board) line units. This collection excludes reinforcements, destroyed/withdrawed
     * units and divided units. Line units are able to perform assaults by themselves
     */
    private Collection<Unit> lineUnits;
    /**
     * Collection of active line-support and support units. This units are not able to perform assaults
     */
    private Collection<Unit> supportUnits;
    private Unit headquarters;
    /**
     * Collection of active service-support units
     */
    private Collection<Unit> serviceUnits;
    /**
     * Collection of scheduled reinforcement units, stored in a queue
     */
    private Queue<Unit> scheduledReinforcements;
    /**
     * Collection of units that could be received as reinforcements, conditioned to certain events
     */
    private Collection<Unit> conditionalReinforcements;
    private Formation superior;
    private Collection<Formation> subordinates;
    private boolean hasPlan;

    // TODO each turn check for reinforcements and put them into the right unit collection
    public Formation(ares.data.jaxb.Formation formation, Force force, Scenario scenario) {
        this.scenario = scenario;
        id = formation.getId();
        name = formation.getName();
        echelon = Echelon.valueOf(formation.getEchelon().name());
        this.force = force;
        commander = formation.getCommander();
        details = formation.getDetails();
        proficiency = formation.getProficiency();
        supply = formation.getSupply();
        orders = Enum.valueOf(OperationType.class, formation.getOrders().name());
        emphasis = formation.getEmphasis();
        supportscope = formation.getSupportscope();
        objectives = new ArrayList<>();
        List<Track> tracks = formation.getTrack();
        Tile[][] tile = scenario.getBoard().getMap();
        if (tracks.size() > 0) {
            for (Objective objective : tracks.get(0).getObjective()) {
                Tile location = tile[objective.getX()][objective.getY()];
                objectives.add(location);
            }
        }
        lineUnits = new ArrayList<>();
        supportUnits = new ArrayList<>();
        serviceUnits = new ArrayList<>();
        scheduledReinforcements = new PriorityQueue<>(2, Unit.UNIT_ENTRY_COMPARATOR);
        conditionalReinforcements = new ArrayList<>();
        subordinates = new ArrayList<>();
        Map<Integer, Unit> allUnits = new HashMap<>();
        for (ares.data.jaxb.Unit unit : formation.getUnit()) {
            Unit u = UnitFactory.getUnit(unit, this, force, scenario);
            allUnits.put(unit.getId(), u);
            switch (u.getAvailability()) {
                case DIVIDED: // divided units are not added to list of units
                    break;
                case TURN:
                    scheduledReinforcements.add(u);
                    break;
                case EVENT:
                    conditionalReinforcements.add(u);
                    break;
                default:
                    switch (u.getType().getCombatClass()) {
                        case LINE:
                            lineUnits.add(u);
                            break;
                        case LINE_SUPPORT:
                        case SUPPORT:
                            supportUnits.add(u);
                            break;

                    }

            }
        }
        // Set parents for units resulting of division
        for (ares.data.jaxb.Unit unit : formation.getUnit()) {
            if (unit.getParent() != null) {
                allUnits.get(unit.getId()).setParent(allUnits.get(unit.getParent()));
            }
        }
    }

    /**
     * Initializes active units and adds formation to clock observers
     *
     * @param clock
     */
    public void initialize() {
        Clock clock = scenario.getClock();
        hasPlan = false;
        for (Unit unit : lineUnits) {
            unit.activate(clock);
        }
        for (Unit unit : supportUnits) {
            unit.activate(clock);
        }
        for (Unit unit : serviceUnits) {
            unit.activate(clock);
        }
        if (headquarters != null) {
            headquarters.activate(clock);
        }
        clock.addFormation(this);
    }

    public void setSuperior(Formation superior) {
        this.superior = superior;
    }

    public void setSubordinates(List<Formation> subordinates) {
        this.subordinates = subordinates;
    }

    public void plan(Clock clock) {
//        if (!hasPlan) {
//            TaskPlanner planner = new TaskPlanner(this, clock);
//            planner.obtainPlan();
//            hasPlan = true;
//        }
        for (Unit unit : lineUnits) {
            if (unit.getMovement() != MovementType.AIRCRAFT) {
                Tile location = unit.getLocation();
                Queue<Action> pendingActions = unit.getPendingActions();
                if (unit.getSpeed() > 0 && unit.getPendingActions().isEmpty() && unit.getEndurance() >= ActionType.APPROACH_MARCH.getWearRate() * clock.MINUTES_PER_TICK) {
                    Action anAction;
                    if (unit.getAction()!= null && unit.getAction().getState() == ActionState.DELAYED) {
                        anAction = new ChangeDeploymentAction(unit, ActionType.ASSEMBLE, location, scenario);
                    } else {
                        Direction[] directions = unit.getLocation().getNeighbors().keySet().toArray(new Direction[0]);
                        int randomDirIndex = RandomGenerator.getInstance().nextInt(directions.length);
                        Direction fromDir = directions[randomDirIndex];
                        anAction = new SurfaceMoveAction((SurfaceUnit) unit, ActionType.TACTICAL_MARCH, location, location.getNeighbors().get(fromDir), clock.getCurrentTime() + 10, fromDir, scenario);
                        
                    }
                    pendingActions.add(anAction);
//                    System.out.println("New Action: " + anAction.toString());
                }
            }
        }
    }

    public Formation getSuperior() {
        return superior;
    }

    public Collection<Formation> getSubordinates() {
        return subordinates;
    }

    public String getCommander() {
        return commander;
    }

    public Collection<Unit> getConditionalReinforcements() {
        return conditionalReinforcements;
    }

    public String getDetails() {
        return details;
    }

    public Echelon getEchelon() {
        return echelon;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public Force getForce() {
        return force;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public OperationType getOrders() {
        return orders;
    }

    public int getProficiency() {
        return proficiency;
    }

    public Queue<Unit> getScheduledReinforcements() {
        return scheduledReinforcements;
    }

    public int getSupply() {
        return supply;
    }

    public SupportScope getSupportscope() {
        return supportscope;
    }

    public List<Tile> getObjectives() {
        return objectives;
    }

    public Collection<Unit> getAllUnits() {
        Collection<Unit> allUnits = new ArrayList<>();
        allUnits.addAll(lineUnits);
        allUnits.addAll(supportUnits);
        allUnits.add(headquarters);
        allUnits.addAll(serviceUnits);
        return allUnits;
    }

    public Collection<Unit> getLineUnits() {
        return lineUnits;
    }

    public Collection<Unit> getSupportUnits() {
        return supportUnits;
    }

    public Unit getHeadquarters() {
        return headquarters;
    }

    public Collection<Unit> getServiceUnits() {
        return serviceUnits;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + id;
        hash = 31 * hash + force.getId();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Formation other = (Formation) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!this.force.equals(other.force)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + name + '}';
    }
}
