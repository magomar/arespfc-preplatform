package ares.model.forces;

import ares.model.scenario.Scenario;
import ares.engine.Clock;
import ares.engine.movement.MovementType;

/**
 *
 * @author Mario Gomez <margomez antiTank dsic.upv.es>
 */
public class AirUnit extends Unit {

    public AirUnit(ares.data.jaxb.Unit unit, Formation formation, Force force, Scenario scenario) {
        super(unit, formation, force, scenario);
        movement = MovementType.AIRCRAFT;
        speed = Integer.MAX_VALUE;
        for (Asset asset : assets) {
            AssetType ast = asset.getType();
            int n = asset.getNumber();
            if (n > 0) {
                speed = Math.min(speed, ast.getSpeed());
            }
        }
    }

    @Override
    public void activate(Clock clock) {
        quality = (2 * proficiency + readiness) / 3;
        efficacy = (2 * proficiency + readiness + supply) / 4;
        endurance = (MAX_ENDURANCE * 200 + MAX_ENDURANCE * readiness + MAX_ENDURANCE * supply) / 400;
        stamina = endurance * 100 / MAX_ENDURANCE;
        maxRange = speed * MAX_ENDURANCE / 90 / 1000;
        range = speed * endurance / 90 / 1000;
        location.add(this);
        clock.addUnit(this);
//        System.out.println("Unit Activated: " + toString());
    }
}
