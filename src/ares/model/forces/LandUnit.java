package ares.model.forces;

import ares.model.scenario.Scenario;
import ares.engine.movement.MovementType;
import java.util.Set;

/**
 *
 * @author Mario Gomez <margomez antiTank dsic.upv.es>
 */
public final class LandUnit extends SurfaceUnit {

    protected int numActiveDefenders;
    protected int numVehicles;
    protected int numHorses;
    protected boolean roadbound;
    protected int engineering;
    protected int majorFerry;
    protected int minorFerry;
    protected int trafficControl;
    protected int logistics;
    protected int command;
    protected int railRepair;
    protected int transportNeeds;
    protected int transportCapacity;

    public LandUnit(ares.data.jaxb.Unit unit, Formation formation, Force force, Scenario scenario) {
        super(unit, formation, force, scenario);
        int transportSpeed = Integer.MAX_VALUE;
        int nonTransportSpeed = Integer.MAX_VALUE;
        int numStatic = 0;
        int numSlow = 0;
        for (Asset asset : assets) {
            AssetType ast = asset.getType();
            int n = asset.getNumber();
            if (n > 0) {
                Set<AssetTrait> assetTraits = ast.getTraits();
                for (AssetTrait t : assetTraits) {
                    switch (t) {
                        case ACTIVE_DEFENDER:
                            numActiveDefenders += n;
                            break;
                        case SLOW:
                            numSlow += n;
                            nonTransportSpeed = Math.min(nonTransportSpeed, ast.getSpeed());
                            break;
                        case STATIC:
                            numStatic += n;
                            break;
                        case TRANSPORT:
                            transportCapacity += n;
                            transportSpeed = Math.min(transportSpeed, ast.getSpeed());
                            break;
                        case MOTORIZED:
                        case SLOW_MOTORIZED:
                        case FAST_MOTORIZED:
                            numVehicles += n;
                            if (!assetTraits.contains(AssetTrait.TRANSPORT)) {
                                nonTransportSpeed = Math.min(nonTransportSpeed, ast.getSpeed());
                            }
                            break;
                        case HORSES:
                        case FAST_HORSES:
                            numHorses += n;
                            if (!assetTraits.contains(AssetTrait.TRANSPORT)) {
                                nonTransportSpeed = Math.min(nonTransportSpeed, ast.getSpeed());
                            }
                            break;
                        case ROADBOUND:
                            roadbound = true;
                            break;
                        case RECON:
                            reconnaissance += n * AssetTrait.RECON.getFactor();
                            break;
                        case ENGINEER:
                            engineering += n * AssetTrait.ENGINEER.getFactor();
                            minorFerry += n * AssetTrait.MINOR_FERRY.getFactor() / 2;
                            break;
                        case RAIL_REPAIR:
                            railRepair += n * AssetTrait.RAIL_REPAIR.getFactor();
                            break;
                        case MAJOR_FERRY:
                            majorFerry += n * AssetTrait.MAJOR_FERRY.getFactor();
                            break;
                        case LOGISTICS:
                            logistics += n;
                            break;
                        case COMMAND:
                            command += n;
                            break;
                        case TRAFFIC_CONTROL:
                            trafficControl += n * AssetTrait.TRAFFIC_CONTROL.getFactor();
                            break;
                        case FIXED:
                            movement = MovementType.FIXED;
                            nonTransportSpeed = 0;
                            break;
                        case RAIL_ONLY:
                            numVehicles += n;
                            movement = MovementType.RAIL;
                            nonTransportSpeed = Math.min(nonTransportSpeed, ast.getSpeed());
                            break;
                        case RIVERINE:
                            numVehicles += n;
                            movement = MovementType.RIVERINE;
                            if (!assetTraits.contains(AssetTrait.TRANSPORT)) {
                                nonTransportSpeed = Math.min(nonTransportSpeed, ast.getSpeed());
                            }
                            break;
                        // TODO airmobile movement
                    }
                }
            }
        }
        transportNeeds = numSlow + numStatic;
        if (type.getCapabilities().contains(Capability.COASTAL_DEFENSE)) {
            movement = MovementType.FIXED;
            speed = 0;
        } else if (movement == null) {
            if (type.getCapabilities().contains(Capability.AMPHIBIOUS)) {
                movement = MovementType.AMPHIBIOUS;
                speed = Math.min(transportSpeed, nonTransportSpeed);
            } else if (transportNeeds == 0) {
                speed = Math.min(transportSpeed, nonTransportSpeed);
                if (numVehicles > 2 * numHorses) {
                    movement = MovementType.MOTORIZED;
                } else if (numVehicles < numHorses) {
                    movement = MovementType.FOOT;
                } else {
                    movement = MovementType.MIXED;
                }
            } else if (transportCapacity > 0) {
                double tr = 2.0 * transportCapacity / transportNeeds;
                if (tr >= 1 && numVehicles > numHorses) {
                    movement = MovementType.MOTORIZED;
                    speed = transportSpeed;
                } else if (tr >= 0.5 && numVehicles > numHorses) {
                    movement = MovementType.MIXED;
                    if (2 * transportCapacity >= numStatic) {
                        speed = (int) Math.max(nonTransportSpeed, tr * transportSpeed);
                    } else {
                        speed = (int) (tr * transportSpeed);
                    }
                } else {
                    movement = MovementType.FOOT;
                    if (2 * transportCapacity >= numStatic) {
                        speed = (int) Math.max(nonTransportSpeed, tr * transportSpeed);
                    } else {
                        speed = (int) (tr * transportSpeed);
                    }
                }
            } else {
                movement = MovementType.FOOT;
                if (nonTransportSpeed == Double.MAX_VALUE) {
                    speed = 0;
                } else {
                    speed = nonTransportSpeed;
                }
            }
        } else {
            speed = Math.min(nonTransportSpeed, transportSpeed);
        }
        speed = (int) (speed * echelon.getSpeedModifier());
    }

    public int getNumVehiclesAndHorses() {
        return numVehicles + numHorses;
    }

    public int getCommand() {
        return command;
    }

    public int getEngineering() {
        return engineering;
    }

    public int getLogistics() {
        return logistics;
    }

    public int getMajorFerry() {
        return majorFerry;
    }

    public int getMinorFerry() {
        return minorFerry;
    }

    public int getNumActiveDefenders() {
        return numActiveDefenders;
    }

    public int getNumHorses() {
        return numHorses;
    }

    public int getNumVehicles() {
        return numVehicles;
    }

    public int getRailRepair() {
        return railRepair;
    }

    public boolean isRoadbound() {
        return roadbound;
    }

    public int getTrafficControl() {
        return trafficControl;
    }

    public int getTransportCapacity() {
        return transportCapacity;
    }

    public int getTransportNeeds() {
        return transportNeeds;
    }


}
