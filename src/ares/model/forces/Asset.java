/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares.model.forces;

import ares.data.jaxb.Unit.Equipment;
import ares.model.scenario.Scenario;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public final class Asset {

    private int number;
    private int max;
    private AssetType type;
   

    public Asset(Equipment e) {
        type = Scenario.ASSET_TYPES.get(e.getName());
        number = e.getNumber();
        max = e.getMax();

        
    }

    public AssetType getType() {
        return type;
    }


    public int getMax() {
        return max;
    }


    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return type.getName() + "(" + number + '/' + max + ')';
    }


}
