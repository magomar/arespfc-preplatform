package ares.model.forces;

import ares.model.scenario.Scenario;
import ares.engine.Clock;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public abstract class SurfaceUnit extends Unit {

    public SurfaceUnit(ares.data.jaxb.Unit unit, Formation formation, Force force, Scenario scenario) {
        super(unit, formation, force, scenario);
    }

    @Override
    public void activate(Clock clock) {
        quality = (2 * proficiency + readiness) / 3;
        efficacy = (2 * proficiency + readiness + supply) / 4;
        endurance = (MAX_ENDURANCE * 200 + MAX_ENDURANCE * readiness + MAX_ENDURANCE * supply) / 400;
        stamina = endurance * 100 / MAX_ENDURANCE;
        maxRange = speed * MAX_ENDURANCE / 90 / 1000;
        range = speed * endurance / 90 / 1000;
        location.add(this);
        clock.addUnit(this);
//        System.out.println("Unit Activated: " + toString());
//        System.out.println(this.name + "\t\tA: " + globalAttack + "\tD: "+ globalDefense);
    }
}
