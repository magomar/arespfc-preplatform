package ares.model.forces;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum Echelon {

    SECTION(0, 0),
    SQUAD(0, 0),
    PLATOON(1, 1),
    COMPANY(3, 2),
    BATTALION(5, 3),
    REGIMENT(15, 4),
    BRIGADE(20, 4),
    DIVISION(40, 5),
    CORPS(80, 6),
    ARMY(120, 7),
    ARMY_GROUP(140, 8),
    REGION(150, 9);
    private final static double SPEED_FACTOR = -0.5;
    private final static double COMMAND_FACTOR = 0.5;
    /**
     * Number of Logistics squads needed to achieve 100% supply distribution
     */
    private final int logisticNeeds;
    /**
     * Number of command squads needed to achieve 100% C2 efficiency
     */
    private final int commandNeeds;
    /**
     * Speed modifier derived from the estimated C2 complexity, which in turn is estimated as a function of the command
     * squads required by an echelon level
     */
    private final double speedModifier;
    /**
     * C2 modifier derived from estimated organization complexity, computed as a function of the command squads required
     * by an echelon level
     */
    private final double commandModifier;

    private Echelon(final int logistics, final int command) {
        this.logisticNeeds = logistics;
        this.commandNeeds = command;
        speedModifier = 1.0 + (command - 3)
                * SPEED_FACTOR / 6.0;
        commandModifier = 1.0 + (command - 3)
                * COMMAND_FACTOR / 6.0;

    }

    public int getCommand() {
        return commandNeeds;
    }

    public double getCommandModifier() {
        return commandModifier;
    }

    public int getLogistics() {
        return logisticNeeds;
    }

    public double getSpeedModifier() {
        return speedModifier;
    }
}
