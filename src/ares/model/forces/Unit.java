package ares.model.forces;

import ares.data.jaxb.Availability;
import ares.data.jaxb.Emphasis;
import ares.data.jaxb.Unit.Equipment;
import ares.engine.Clock;
import ares.engine.Scale;
import ares.engine.action.AbstractAction;
import ares.engine.action.Action;
import ares.engine.action.ActionState;
import ares.engine.action.OpState;
import ares.engine.action.RestAction;
import ares.engine.action.WaitAction;
import ares.engine.movement.MovementType;
import ares.model.board.Board;
import ares.model.board.Tile;
import ares.model.scenario.Scenario;
import java.util.*;

/**
 *
 * @author Mario Gomez <margomez antiTank dsic.upv.es>
 */
public abstract class Unit {

    public static final Comparator<Unit> UNIT_ACTION_FINISH_COMPARATOR = new UnitActionFinishComparator();
    public static final Comparator<Unit> UNIT_ENTRY_COMPARATOR = new UnitEntryComparator();
    public static final int MAX_ENDURANCE = 18 * 60 * 60;
    protected Scenario scenario;
    protected int id;
    protected String name;
    protected int iconId;
    protected int color;
    protected UnitType type;
    protected Echelon echelon;
    protected Formation formation;
    protected Force force;
//    protected Experience experience;
    protected int proficiency;
    protected int readiness;
    protected int supply;
    protected Tile location;
    protected Emphasis emphasis;
    protected Availability availability;
    protected OpState opState;
    protected int replacementPriority;
    protected int entry;
    protected Unit parent;
    protected Collection<Asset> assets;
    //computed attributes
    protected int antiTank;
    protected int antiPersonnel;
    protected int highAntiAir;
    protected int lowAntiAir;
    protected int defense;
    protected int artillery;
    /**
     * Range for indirect fire, specified in Km *
     */
    protected int artilleryRange;
    protected int weight;
    protected MovementType movement;
    /**
     * Standard average moving speed in ideal conditions, specified in meters per minute
     */
    protected int speed;
    protected int quality;
    protected int efficacy;
    protected int reconnaissance;
    protected Action action;
    protected Queue<Action> pendingActions;
    /**
     * Represents the remaining physical resistence of a unit before becoming exhausted, expressed in action points
     * (seconds of low-intensity activity). The execution of actions consumes endurance, but it can be replenished by
     * resting.
     */
    protected int endurance;
    /**
     * Percentual representation of endurance (endurance * 100 / MAX_ENDURANCE)
     */
    protected int stamina;
    /**
     * Distance in meters a unit is currently able to move antiTank standard average speed, before becoming exhausted.
     */
    protected int range;
    /**
     * Distance in meters a rested unit would be able to move in ideal conditions at standard average speed before
     * becoming exhausted.
     */
    protected int maxRange;
    // TODO maps with all cumulated traits
//    protected Map<AssetTrait, Integer> traits;
    protected int attackStrength;
    protected int defenseStrength;
    protected int health;
    
    protected boolean selected;

    protected Unit(ares.data.jaxb.Unit unit, Formation formation, Force force, Scenario scenario) {
        this.scenario = scenario;
        id = unit.getId();
        name = unit.getName();
        type = UnitType.valueOf(unit.getType().name());
        if (type.ordinal() > 114) {
            System.err.println(unit.getName() + " *** " + type);
        }
        iconId = unit.getIconId();
        color = unit.getColor();
        echelon = Echelon.valueOf(unit.getSize().name());
        this.formation = formation;
        this.force = force;
        proficiency = unit.getProficiency();
        readiness = unit.getReadiness();
        supply = unit.getSupply();
        emphasis = unit.getEmphasis();
        availability = unit.getAvailability();
        opState = OpState.valueOf(unit.getOpState().name());
        replacementPriority = unit.getReplacementPriority();
        entry = (availability == Availability.TURN || availability == Availability.EVENT ? entry = unit.getEntry() : 1);
        artilleryRange = 0;
        assets = new ArrayList<>();
        selected = false;
        boolean indirectFireSupport = type.getCapabilities().contains(Capability.BOMBARDMENT);
        for (Equipment equip : unit.getEquipment()) {
            Asset a = new Asset(equip);
            assets.add(a);
            AssetType ast = a.getType();
            int n = a.getNumber();
            if (n > 0) {
                antiTank += n * ast.getAt();
                antiPersonnel += n * ast.getAp();
                defense += n * ast.getDf();
                highAntiAir += n * ast.getAah();
                lowAntiAir += n * ast.getAal();
                if (indirectFireSupport) {
                    int astRange = ast.getArtyRange();
                    if (astRange > 0) {
                        artilleryRange = (artilleryRange == 0 ? astRange : Math.min(artilleryRange, astRange));
                        artillery += n * ast.getAp() / 2;
                    }
                }
                weight += n * ast.getWeight();
            }
        }
        Board board = scenario.getBoard();
        Scale scale = scenario.getScale();
        int x = unit.getX();
        int y = unit.getY();
        location = board.getMap()[x][y];

        pendingActions = new PriorityQueue<>(4, AbstractAction.ACTION_START_COMPARATOR);

        attackStrength = (int) (efficacy * (antiTank + antiPersonnel) / scale.getArea());
        defenseStrength = (int) (efficacy * defense / scale.getArea());
        health = (int) (efficacy - 1 / 20);
    }

    /**
     * This method makes the unit active, which implies initializing state attributes, placing the unit in the board and
     * adding it as observer of the scenario clock, so as to be updated every tick of the clock
     *
     * @param clock
     */
    public abstract void activate(Clock clock);

    public void schedule(Clock clock) {
        if (action != null && (action.getState() == ActionState.COMPLETED || action.getState() == ActionState.ABORTED)) {
            action = null;
        }
        if (action == null) {
            if (pendingActions.size() > 0) {
                Action nextAction = pendingActions.peek();
                int actionStart = nextAction.getStart();
                if (actionStart < clock.getCurrentTime() + clock.MINUTES_PER_TICK) {
                    action = pendingActions.poll();
                    Tile destination = action.getDestination();
                    destination.putAction(action);
                }
            } else {
                if (endurance > 0) {
                    action = new WaitAction(this, location, scenario);
                } else {
                    action = new RestAction(this, location, scenario);
                }
            }
        }
    }

    public void act(Clock clock) {
        if (action != null) {
            action.execute();
            range = (int) (speed * endurance);
        }
        if (clock.isNewDay()) {
            quality = (2 * proficiency + readiness) / 3;
            efficacy = (2 * proficiency + readiness + supply) / 4;
            int enduranceRestored = (MAX_ENDURANCE * 200 + MAX_ENDURANCE * readiness + MAX_ENDURANCE * supply) / 400;
            endurance = Math.min(endurance + enduranceRestored, enduranceRestored);
            stamina = endurance * 100 / MAX_ENDURANCE;
            maxRange = speed * MAX_ENDURANCE / 90 / 1000;
            range = speed * endurance / 90 / 1000;
            Scale scale = scenario.getScale();
            attackStrength = (int) (efficacy * (antiTank + antiPersonnel) / scale.getArea());
            defenseStrength = (int) (efficacy * defense / scale.getArea());
            health = (int) (efficacy - 1 / 20);
        }
    }

    public void wear(int wear) {
        endurance -= wear;
        stamina = endurance * 100 / MAX_ENDURANCE;
    }

    public void setParent(Unit parent) {
        this.parent = parent;
    }

    public void setLocation(Tile location) {
        this.location = location;
    }

    public void setOpState(OpState opState) {
        this.opState = opState;
    }

    public Tile getLocation() {
        return location;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public int getRange() {
        return range;
    }

    public int getStamina() {
        return stamina;
    }

    public int getId() {
        return id;
    }

    public int getHighAntiAir() {
        return highAntiAir * efficacy;
    }

    public int getLowAntiAir() {
        return lowAntiAir * efficacy;
    }

    public int getAntiPersonnel() {
        return antiPersonnel * efficacy;
    }

    public int getArtillery() {
        return artillery * efficacy;
    }

    public Collection<Asset> getAssets() {
        return assets;
    }

    public int getAntiTank() {
        return antiTank * efficacy;
    }

    public int getColor() {
        return color;
    }

    public int getDefense() {
        return defense * efficacy;
    }

    public int getArtilleryRange() {
        return artilleryRange;
    }

    public int getEndurance() {
        return endurance;
    }

    public Queue<Action> getPendingActions() {
        return pendingActions;
    }

    public Action getAction() {
        return action;
    }

    public int getEfficacy() {
        return efficacy;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public int getEntry() {
        return entry;
    }

    public int getIconId() {
        return iconId;
    }

    public String getName() {
        return name;
    }

    public int getReconnaissance() {
        return reconnaissance;
    }

    public Unit getParent() {
        return parent;
    }

    public int getProficiency() {
        return proficiency;
    }

    public int getQuality() {
        return quality;
    }

    public int getReadiness() {
        return readiness;
    }

    public int getReplacementPriority() {
        return replacementPriority;
    }

    public Echelon getSize() {
        return echelon;
    }

    public int getSpeed() {
        return speed;
    }

    public Availability getAvailability() {
        return availability;
    }

    public int getSupply() {
        return supply;
    }

    public UnitType getType() {
        return type;
    }

    public int getWeight() {
        return weight;
    }

    public MovementType getMovement() {
        return movement;
    }

    public Echelon getEchelon() {
        return echelon;
    }

    public Force getForce() {
        return force;
    }

    public Formation getFormation() {
        return formation;
    }

    public OpState getOpState() {
        return opState;
    }

    public int getAttackStrength() {
        return attackStrength;
    }

    public int getDefenseStrength() {
        return defenseStrength;
    }

    public int getHealth() {
        return health;
    }
    
    public boolean isSelected(){
        return selected;
    }
    
    public void setSelected(boolean _selected){
        this.selected = _selected;
    }

    protected static class UnitActionFinishComparator implements Comparator<Unit> {

        @Override
        public int compare(Unit u1, Unit u2) {
            Action a1 = u1.getAction();
            Action a2 = u2.getAction();
            if (a1 == null) {
                if (a2 == null) {
                    return 0;
                } else {
                    return 1;
                }
            } else if (a2 == null) {
                return -1;
            } else {
                return a1.getFinish() - a2.getFinish();
            }
        }
    }

    protected static class UnitEntryComparator implements Comparator<Unit> {

        @Override
        public int compare(Unit u1, Unit u2) {
            int entry1 = u1.getEntry();
            int entry2 = u2.getEntry();
            return entry1 - entry2;
        }
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + id;
//        hash = 31 * hash + formation.getId();
        hash = 31 * hash + force.getId();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Unit other = (Unit) obj;
        if (this.id != other.id) {
            return false;
        }
//        if (!this.formation.equals(other.formation)) {
//            return false;
//        }
        if (!this.force.equals(other.force)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name + "(" + type.name() + ")." + movement + "." + opState + "[" + stamina + "]" + " @ " + location;
    }

    public String toStringLong() {
        return toString() + '{' + echelon + ", " + formation + ", " + force + ", location=" + location + ", speed=" + speed + ", assets=" + assets + '}';
    }
}
