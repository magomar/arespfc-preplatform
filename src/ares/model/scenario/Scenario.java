package ares.model.scenario;

import ares.data.jaxb.EquipmentDB;
import ares.data.jaxb.EquipmentDB.EquipmentCategory;
import ares.data.jaxb.EquipmentDB.EquipmentCategory.Item;
import ares.data.jaxb.OOB;
import ares.engine.Clock;
import ares.engine.Scale;
import ares.model.board.Board;
import ares.model.forces.AssetType;
import ares.model.forces.Force;
import ares.model.forces.Unit;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Class containing all the state of a scenario
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public final class Scenario {

    public static final Map<String, AssetType> ASSET_TYPES = new HashMap<>();
    private Board board;
    private Scale scale;
    private Force[] forces;
    private Clock clock;
    private ScenarioInfo gameInfo;

    public Scenario(ares.data.jaxb.Scenario scenario, EquipmentDB eqpDB) {

        scale = new Scale((int) (scenario.getEnvironment().getScale() * 1000));
        clock = new Clock(scenario.getCalendar());
        gameInfo = new ScenarioInfo(scenario.getHeader(), clock);
        for (EquipmentCategory ec : eqpDB.getEquipmentCategory()) {
            for (Item it : ec.getItem()) {
                ASSET_TYPES.put(it.getName(), new AssetType(it));
            }
        }
        board = new Board(scenario);
        OOB oob = scenario.getOOB();
        Collection<ares.data.jaxb.Force> scenForces = oob.getForce();
        forces = new Force[scenForces.size()];
        for (ares.data.jaxb.Force force : oob.getForce()) {
            forces[force.getId() - 1] = new Force(force, this);
            //TODO modify ToawToAres to make force indexes in [0.. n-1] instead of substracting 1 here
        }
        for (int i = 0; i < forces.length; i++) {
            forces[i].initialize();
        }
        board.initialize(scenario, this, forces);

        System.out.println("Game loaded: " + toString());
    }

    public void start() {
        clock.start();
    }

    public void stop() {
        clock.stop();
    }

    public Board getBoard() {
        return board;
    }

    public Force[] getForces() {
        return forces;
    }

    public Scale getScale() {
        return scale;
    }

    public Clock getClock() {
        return clock;
    }

    public ScenarioInfo getGameInfo() {
        return gameInfo;
    }

    public List<Unit> getActiveUnits() {
        return clock.getUnits();
    }

    @Override
    public String toString() {
        return "Game{" + "Scale=" + scale + ", clock=" + clock + ", turn length=" + clock.getTurnLength() + '}';
    }
}
