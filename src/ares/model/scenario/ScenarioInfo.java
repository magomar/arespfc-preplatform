package ares.model.scenario;

import ares.data.jaxb.Header;
import ares.engine.Clock;
import java.util.GregorianCalendar;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class ScenarioInfo {

    private String name;
    private GregorianCalendar begins;
    private GregorianCalendar ends;

    public ScenarioInfo(Header header, Clock clock) {
        name = header.getName();
        begins = clock.getBegins();
        ends = clock.getEnds();
    }

    public String getName() {
        return name;
    }

    public GregorianCalendar getBegins() {
        return begins;
    }

    public GregorianCalendar getEnds() {
        return ends;
    }


}
