package ares.engine;

import ares.gui.UnitsPanel;
import ares.model.forces.Formation;
import ares.model.forces.Unit;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class Clock implements Runnable {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private final GregorianCalendar begins;
    private final GregorianCalendar ends;
    private final GregorianCalendar now;
    private final int finalTurn;
    public final int MINUTES_PER_DAY = 24 * 60 * 60;
    public final int TICKS_PER_TURN;
    public final int TICKS_PER_DAY;
    public final int TICKS_PER_HOUR;
    public final int MINUTES_PER_TICK;
    public final int MINUTES_PER_TURN;
    public final int MILLISEC_PER_TICK;
    public final int MILLISEC_PER_TURN;
    private final TurnLength turnLength; // Probably this could be removed, but not yet
    private ScheduledExecutorService scheduledExecutorService;
    // minutes transcurred since the beginning of the scenario
    private int currentTime;
    private long period = 100;
    private int tick;
    private int turn;
    private boolean finished;
    private boolean newDay;
    private boolean changed = false;
    private List<Unit> units;
    private List<Formation> formations;
    private UnitsPanel unitsPanel;
    private Phase phase;
    private boolean isPaused;
    private boolean isStarted;

    public Clock(ares.data.jaxb.Calendar calendar) {
        turnLength = TurnLength.valueOf(calendar.getTurnLength().name());
        MINUTES_PER_TICK = turnLength.getMinutesPerTick();
        MINUTES_PER_TURN = turnLength.getMinutes();
        MILLISEC_PER_TICK = MINUTES_PER_TICK * 60000;
        MILLISEC_PER_TURN = MINUTES_PER_TURN * 60000;
        TICKS_PER_TURN = MINUTES_PER_TURN / MINUTES_PER_TICK;
        TICKS_PER_DAY = 1440 / MINUTES_PER_TICK;
        TICKS_PER_HOUR = 60 / MINUTES_PER_TICK;
        GregorianCalendar gcal = calendar.getStartDate().toGregorianCalendar();
        gcal.set(GregorianCalendar.HOUR_OF_DAY, 6);
        gcal.set(GregorianCalendar.MINUTE, 0);
        begins = (GregorianCalendar) gcal.clone();
        turn = calendar.getCurrentTurn();
        tick = TICKS_PER_TURN - 1;
        gcal.add(GregorianCalendar.MINUTE, turn * MINUTES_PER_TURN - MINUTES_PER_TICK);
        now = (GregorianCalendar) gcal.clone();
        currentTime = turn * MINUTES_PER_TURN - MINUTES_PER_TICK;
        finalTurn = calendar.getFinalTurn();
        ends = (GregorianCalendar) begins.clone();
        ends.add(GregorianCalendar.MINUTE, finalTurn * MINUTES_PER_TURN);
        finished = false;
        newDay = false;
        changed = false;
        units = new ArrayList<>();
        formations = new ArrayList<>();
        scheduledExecutorService = Executors.newScheduledThreadPool(5);
        phase = Phase.ACT;
        isPaused = true;
        isStarted = false;
        System.out.println("*** Game start " + DATE_FORMAT.format(begins.getTime()));
        System.out.println("*** Game end " + DATE_FORMAT.format(ends.getTime()));
    }

    public void start() {
        Logger.getLogger(Clock.class.getName()).log(Level.INFO, "*** Game Started", DATE_FORMAT.format(now.getTime()));
        Logger.getLogger(Clock.class.getName()).log(Level.INFO, "*** Current time in minutes =  {0}", currentTime);
        if (!isStarted) {
            isStarted = true;
        }
        isPaused = false;
        ScheduledFuture scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(this, 0, period, TimeUnit.MILLISECONDS);
    }

    public void stop() {
        isPaused = true;
        try {
            scheduledExecutorService.awaitTermination(period, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Clock.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(Clock.class.getName()).log(Level.INFO, "*** Game Stopped", DATE_FORMAT.format(now.getTime()));
    }

    @Override
    public void run() {
        if (phase == Phase.ACT) {
            now.add(GregorianCalendar.MINUTE, MINUTES_PER_TICK);
            currentTime += MINUTES_PER_TICK;
            tick++;
            if (newDay == false) {
                if (now.get(GregorianCalendar.HOUR_OF_DAY) == 6 && now.get(GregorianCalendar.MINUTE) == 0) {
                    newDay = true;
                }
            } else {
                newDay = false;
            }
            if (tick == TICKS_PER_TURN) {
                tick = 0;
                turn++;
                if (turn > finalTurn) {
                    finished = true;
                } else {
                    System.out.println("Turn: " + turn + " - Time: " + toString());
                }
            }

            Collections.sort(units, Unit.UNIT_ACTION_FINISH_COMPARATOR);
//            System.out.println("tick: " + tick + " - Time: " + toString());
        }
        setChanged();
        notifyObservers();
        phase = phase.getNext();
        if (finished) {
            System.out.println("*** End of scenario *** " + toString());
            stop();
        }
    }

    public void notifyObservers() {
        // a temporary array buffer, used as a snapshot of the state of current Observers.
        Unit[] arrLocal;
        Formation[] arrLocal2;
        synchronized (this) {
            if (!changed) {
                return;
            }
            arrLocal = units.toArray(new Unit[0]);
            arrLocal2 = formations.toArray(new Formation[0]);
            clearChanged();
        }
        switch (phase) {
            case SCHEDULE:
                for (int i = arrLocal.length - 1; i >= 0; i--) {
                    arrLocal[i].schedule(this);
                }
                if (unitsPanel != null) {
                    unitsPanel.updateUnits();
                }
                break;
            case ACT:
                for (int i = arrLocal.length - 1; i >= 0; i--) {
                    arrLocal[i].act(this);
                }
                for (int i = arrLocal2.length - 1; i >= 0; i--) {
                    arrLocal2[i].plan(this);
                }
                break;
        }
    }

    public synchronized void addUnit(Unit unit) {
        units.add(unit);
    }

    public synchronized void addUnits(Collection<Unit> units) {
        this.units.addAll(units);
    }

    public synchronized void addFormation(Formation formation) {
        formations.add(formation);
    }

    public synchronized void addFormations(Collection<Formation> formations) {
        this.formations.addAll(formations);
    }

    public synchronized void removeUnit(Unit observer) {
        units.remove(observer);
    }

    protected synchronized void setChanged() {
        changed = true;
    }

    protected synchronized void clearChanged() {
        changed = false;
    }

    public synchronized boolean hasChanged() {
        return changed;
    }

    public synchronized int countObservers() {
        return units.size();
    }

    public void setUnitsPanel(UnitsPanel unitsPanel) {
        this.unitsPanel = unitsPanel;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setPeriod(long period) {
        this.period = period;
    }

    public GregorianCalendar getBegins() {
        return begins;
    }

    public GregorianCalendar getEnds() {
        return ends;
    }

    public int getFinalTurn() {
        return finalTurn;
    }

    public long getPeriod() {
        return period;
    }

    public int getTick() {
        return tick;
    }

    public int getTurn() {
        return turn;
    }

    public TurnLength getTurnLength() {
        return turnLength;
    }

    public GregorianCalendar getNow() {
        return now;
    }

    public int getCurrentTime() {
        return currentTime;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public boolean isNewDay() {
        return newDay;
    }

    public List<Formation> getFormations() {
        return formations;
    }
    
    @Override
    public String toString() {
        return DATE_FORMAT.format(now.getTime());
    }
}
