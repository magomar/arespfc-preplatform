package ares.engine.planner.tacticalmission;

import ares.model.scenario.Scenario;
import ares.engine.planner.UnitTaskNode;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public interface TacticalMission {
    
    /**
     * 
     * @return the estimated time to complete, in minutes
     */
    public int plan(UnitTaskNode taskNode);
    //TODO Include position tile
}
