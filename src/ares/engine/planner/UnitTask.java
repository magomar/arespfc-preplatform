package ares.engine.planner;

import ares.engine.command.TacticalMissionType;
import ares.model.board.Tile;

/**
 *
 * @author Saúl Esteban
 */
public class UnitTask extends Task{
    
    private Tile location;
    private TacticalMissionType tacticalMissionType;
    
    public UnitTask(TacticalMissionType tm, Tile p, Tile g) {
        super(g);
        location = p;
        tacticalMissionType = tm;
    }
    
    public Tile getLocation() {
        return location;
    }
    
    public TacticalMissionType getTacticalMissionType() {
        return tacticalMissionType;
    }
}
