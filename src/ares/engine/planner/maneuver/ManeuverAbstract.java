package ares.engine.planner.maneuver;

import ares.engine.command.OperationForm;
import ares.engine.planner.FormationTaskNode;
import ares.model.board.Tile;
import ares.model.forces.Formation;
import java.util.LinkedList;

/**
 *
 * @author Saúl Esteban
 */
public abstract class ManeuverAbstract implements Maneuver{
    
    protected LinkedList<OperationForm> operations;
    protected FormationTaskNode node;
    
    @Override
    public void setData(FormationTaskNode node) {
        this.node = node;
    }
    
    @Override
    public LinkedList<OperationForm> getOperations() {
        return operations;
    }
}
