package ares.engine.action;

import ares.model.scenario.Scenario;
import ares.engine.Clock;
import ares.model.board.Tile;
import ares.model.forces.Unit;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class RestAction extends AbstractAction {

    public RestAction(Unit unit, Tile destination, int start, Scenario scenario) {
        super(unit, ActionType.REST, destination, destination, start, scenario);
    }

    public RestAction(Unit unit, Tile destination, Scenario scenario) {
        this(unit, destination, scenario.getClock().getCurrentTime(), scenario);
    }

    @Override
    public void execute() {
        Clock clock = scenario.getClock();
        start = Math.max(start, clock.getCurrentTime() - clock.MINUTES_PER_TICK);
        int duration = clock.MINUTES_PER_TICK;
        int wear = (int) (type.getWearRate() * duration);
        unit.wear(wear);
        state = ActionState.COMPLETED;
        finish = clock.getCurrentTime();
//        System.out.println("[" + clock + "] -> " + this.toString());
    }

    @Override
    public String toString() {
        return unit.toString() + "RESTED #" + id + " @ " + origin.getX() + "," + origin.getY()
                + " (" + start + "->" + finish + ")";
    }
}
