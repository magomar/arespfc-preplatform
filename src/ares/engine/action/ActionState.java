package ares.engine.action;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum ActionState {
    CREATED,
    DELAYED,
    INITIATED,
    COMPLETED,
    ABORTED;
}
