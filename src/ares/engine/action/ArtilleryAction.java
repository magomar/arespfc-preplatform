package ares.engine.action;

import ares.model.scenario.Scenario;
import ares.model.board.Tile;
import ares.model.forces.Unit;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class ArtilleryAction extends AbstractAction {

    public ArtilleryAction(Unit unit, ActionType type, Tile origin, Tile destination, int start, Scenario scenario) {
        super(unit, type, origin, destination, start, scenario);
    }

    public ArtilleryAction(Unit unit, ActionType type, Tile origin, Tile destination, Scenario scenario) {
        this(unit, type, origin, destination, scenario.getClock().getCurrentTime(), scenario);
    }

    @Override
    public void execute() {
        System.out.println(unit + "Bombarding");
    }
}
