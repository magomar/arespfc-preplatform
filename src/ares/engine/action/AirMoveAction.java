package ares.engine.action;

import ares.model.scenario.Scenario;
import ares.engine.Clock;
import ares.model.board.Direction;
import ares.model.board.Tile;
import ares.model.forces.AirUnit;
import java.util.List;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class AirMoveAction extends MoveAction {

    public AirMoveAction(AirUnit unit, ActionType type, Tile origin, Tile destination, int start, Direction fromDir, Scenario scenario) {
        super(unit, type, origin, destination, start, fromDir, scenario);
    }

    public AirMoveAction(AirUnit unit, ActionType type, Tile origin, Tile destination, Direction fromDir, Scenario scenario) {
        this(unit, type, origin, destination, scenario.getClock().getCurrentTime(), fromDir, scenario);
    }

    @Override
    public void execute() {
        Clock clock = scenario.getClock();
        if (checkPreconditions()) {
            List<AirUnit> unitsInDestination = (List<AirUnit>) destination.getAirUnits();
            int duration;
            if (unitsInDestination.size() > 0 && !unitsInDestination.get(0).getForce().equals(unit.getForce())) {
                duration = clock.MINUTES_PER_TICK;
                finish = clock.getCurrentTime();
                state = ActionState.ABORTED;
                System.out.println("[" + clock + "] -> " + "ABORTED " + this.toString());
            } else {
                if (timeToComplete > clock.MINUTES_PER_TICK) {
                    duration = clock.MINUTES_PER_TICK;
                    timeToComplete -= duration;
                } else {
                    duration = timeToComplete;
                    timeToComplete = 0;
                    state = ActionState.COMPLETED;
                    finish = clock.getCurrentTime() - clock.MINUTES_PER_TICK + duration;
                    origin.remove(unit);
                    destination.add(unit);
                    unit.setLocation(destination);
                    System.out.println("[" + clock + "] -> " + "COMPLETED " + this.toString());
                }
            }
            int wear = (int) (type.getWearRate() * duration);
            unit.wear(wear);
        }
    }

    @Override
    public String toString() {
        return unit.toString() + " from " + origin + " to " + destination + " at " + (speed * 60.0 / 1000) + " km/h";
    }
}
