package ares.engine.action;

import ares.model.scenario.Scenario;
import ares.engine.Clock;
import ares.model.board.Tile;
import ares.model.forces.Unit;

/**
 * Actions that change between static (deployed) and mobile status. There are two types of actions in this category:
 * Assemble and Deploy
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class ChangeDeploymentAction extends AbstractAction {

    public ChangeDeploymentAction(Unit unit, ActionType type, Tile destination, int start, Scenario scenario) {
        super(unit, type, destination, destination, start, scenario);
        finish = (int) (start + 15 * unit.getEchelon().getSpeedModifier());
        timeToComplete = finish - start;
    }

    public ChangeDeploymentAction(Unit unit, ActionType type, Tile destination, Scenario scenario) {
        this(unit, type, destination, scenario.getClock().getCurrentTime(), scenario);
    }

    @Override
    public void execute() {
        Clock clock = scenario.getClock();
        if (checkPreconditions()) {
            int duration;
            if (timeToComplete > clock.MINUTES_PER_TICK) {
                duration = clock.MINUTES_PER_TICK;
                timeToComplete -= duration;
                System.out.println("[" + clock + "] -> " + "ONGOING " + this.toString());
            } else {
                duration = timeToComplete;
                timeToComplete = 0;
                state = ActionState.COMPLETED;
                finish = clock.getCurrentTime() - clock.MINUTES_PER_TICK + duration;
                unit.setOpState(type.getEffectAfter());
                int wear = (int) (type.getWearRate() * duration);
                unit.wear(wear);
                System.out.println("[" + clock + "] -> " + "COMPLETED " + this.toString());
            }
        } else {
            System.out.println("[" + clock + "] -> " + "DELAYED " + this.toString());
        }
    }

    @Override
    public String toString() {
        return unit.toString() + "ChangeDeployment #" + id + " @ " + origin.getX() + "," + origin.getY()
                + " from " + type.getPrecondition() + " to" + type.getEffectAfter() + " (" + start + "->" + finish + ")";
    }
}