package ares.engine.action;

import ares.model.board.Direction;
import ares.model.board.Tile;
import ares.model.forces.Unit;
import ares.model.scenario.Scenario;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public abstract class MoveAction extends AbstractAction {

    /**
     * Direction of the movement, relative to the origin of the movement
     */
    protected Direction fromDir;
    /**
     * Actual speed for this particular action, having into account not just the base speed and movement costs
     * occasioned by the terrain, which are all precomputed for every unit and tile respectively, but also the dinamic
     * aspects of the scenario, such as the traffic density (a function of the number of horses and vehicles) and the
     * presence of enemy units in the vicinity
     */
    protected int speed;

    public MoveAction(Unit unit, ActionType type, Tile origin, Tile destination, int start, Direction fromDir, Scenario scenario) {
        super(unit, type, origin, destination, start, scenario);
        this.fromDir = fromDir;
        int cost = origin.getMoveCost(fromDir).getActualCost(unit, destination, fromDir);
        speed = unit.getSpeed() / cost;
        int distance = scenario.getScale().getDistance();
        timeToComplete = (speed > 0 ? (int) (distance / speed) : Integer.MAX_VALUE);
    }

    public MoveAction(Unit unit, ActionType type, Tile origin, Tile destination, Direction fromDir, Scenario scenario) {
        this(unit, type, origin, destination, scenario.getClock().getCurrentTime(), fromDir, scenario);
    }

    /**
     *
     * @return the actual speed of the unit when performing this movement (m/m)
     */
    public int getSpeed() {
        return speed;
    }
}
