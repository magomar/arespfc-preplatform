package ares.engine.action;

import ares.model.scenario.Scenario;
import ares.engine.Clock;
import ares.model.board.Tile;
import ares.model.forces.Unit;
import java.util.Comparator;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public abstract class AbstractAction implements Action {

    public static final Comparator<Action> ACTION_START_COMPARATOR = new ActionStartComparator();
    public static final Comparator<Action> ACTION_FINISH_COMPARATOR = new ActionFinishComparator();
    protected Unit unit;
    protected Tile origin;
    protected Tile destination;
    protected ActionType type;
    protected int id;
    protected Scenario scenario;
    /**
     * Before starting the action, this attribute holds the estimated time to start performing the action, specified in
     * minutes since the beginning of the scenario. Thereafter it holds the actual starting time
     */
    protected int start;
    /**
     * Before finishing the action, this attribute holds the estimated time to complete the action, specified in minutes
     * since the scenario begun. Thereafter it holds the actual finishing time
     */
    protected int finish;
    /**
     * Estimated remaining time to complete the action in minutes
     */
    protected int timeToComplete;
    protected ActionState state;

    public AbstractAction(Unit unit, ActionType type, Tile origin, Tile destination, int start, Scenario scenario) {
        this.scenario = scenario;
        this.unit = unit;
        this.type = type;
        this.start = start;
        this.origin = origin;
        this.destination = destination;
        Clock clock = scenario.getClock();
        finish = clock.getFinalTurn() * clock.MINUTES_PER_TURN;
        timeToComplete = finish - start;
        id = clock.getCurrentTime();
        state = ActionState.CREATED;
    }

//    public AbstractAction(Unit unit, ActionType type, Tile origin, Tile destination, Clock clock) {
//        this(unit, type, origin, destination, clock.getCurrentTime(), clock);
//    }
//
//    public AbstractAction(Unit unit, ActionType type, Tile destination, int start, Clock clock) {
//        this(unit, type, destination, destination, start, clock);
//    }
//
//    public AbstractAction(Unit unit, ActionType type, Tile destination, Clock clock) {
//        this(unit, type, destination, destination, clock.getCurrentTime(), clock);
//    }
    protected boolean checkPreconditions() {
        Clock clock = scenario.getClock();
        if (unit.getEndurance() <= 0) {
            state = ActionState.DELAYED;
            unit.getPendingActions().add(this);
            Action newAction = new RestAction(unit, destination, scenario);
            newAction.execute();
            return false;
        }
        if (state == ActionState.CREATED && unit.getOpState() != type.getPrecondition()) {
            state = ActionState.DELAYED;
            unit.getPendingActions().add(this);
            Action newAction;
            if (unit.getEndurance() >= ActionType.WAIT.getWearRate()) {
                newAction = new WaitAction(unit, destination, scenario);
            } else {
                newAction = new RestAction(unit, destination, scenario);
            }
            newAction.execute();
            return false;
        }
        if (unit.getLocation() != origin) {
            Action newAction;
            if (unit.getEndurance() >= ActionType.WAIT.getWearRate()) {
                newAction = new WaitAction(unit, destination, scenario);
            } else {
                newAction = new RestAction(unit, destination, scenario);
            }
            newAction.execute();
            return false;
        }

        if (state == ActionState.CREATED || state == ActionState.DELAYED) {
            state = ActionState.INITIATED;
            start = Math.max(start, clock.getCurrentTime() - clock.MINUTES_PER_TICK);
            unit.setOpState(type.getEffectWhile());
        }
        return true;
    }

    @Override
    public void setStart(int s) {
        start = s;
        timeToComplete = finish - start;
    }

    @Override
    public ActionState getState() {
        return state;
    }

    @Override
    public Tile getOrigin() {
        return origin;
    }

    @Override
    public Tile getDestination() {
        return destination;
    }

    @Override
    public int getStart() {
        return start;
    }

    @Override
    public int getFinish() {
        return finish;
    }

    @Override
    public ActionType getType() {
        return type;
    }

    @Override
    public Unit getUnit() {
        return unit;
    }

    public int getTimeToComplete() {
        return timeToComplete;
    }

    @Override
    public String toString() {
        return "Action{" + "unit=" + unit + ", type=" + type + ", start=" + start + '}';
    }

    private static class ActionStartComparator implements Comparator<Action> {

        @Override
        public int compare(Action o1, Action o2) {
            int start1 = o1.getStart();
            int start2 = o2.getStart();
            int diff = start1 - start2;
            return (diff == 0 ? o1.getFinish() - o2.getStart() : diff);
        }
    }

    private static class ActionFinishComparator implements Comparator<Action> {

        @Override
        public int compare(Action o1, Action o2) {
            int finish1 = o1.getFinish();
            int finish2 = o2.getFinish();
            int diff = finish1 - finish2;
            return (diff == 0 ? o1.getStart() - o2.getStart() : diff);
        }
    }
}