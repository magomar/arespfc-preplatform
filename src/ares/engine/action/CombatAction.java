package ares.engine.action;

import ares.model.scenario.Scenario;
import ares.model.board.Direction;
import ares.model.board.Tile;
import ares.model.forces.Unit;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class CombatAction extends MoveAction {

    public CombatAction(Unit unit, ActionType type, Tile origin, Tile destination, int start, Direction fromDir, Scenario scenario) {
        super(unit, type, origin, destination, start, fromDir, scenario);
    }

    public CombatAction(Unit unit, ActionType type, Tile origin, Tile destination, Direction fromDir, Scenario scenario) {
        this(unit, type, origin, destination, scenario.getClock().getCurrentTime(), fromDir, scenario);
    }

    @Override
    public void execute() {
        System.out.println(unit + "Attacking");
    }
}
