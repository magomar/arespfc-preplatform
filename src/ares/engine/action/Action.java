package ares.engine.action;

import ares.model.board.Tile;
import ares.model.forces.Unit;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public interface Action {

    public void execute();
    
    public void setStart(int s);

    public Unit getUnit();

    public ActionType getType();

    public int getStart();

    public int getFinish();

    public Tile getOrigin();

    public Tile getDestination();

    public ActionState getState();
}
