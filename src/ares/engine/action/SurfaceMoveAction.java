package ares.engine.action;

import ares.model.scenario.Scenario;
import ares.engine.Clock;
import ares.model.board.Direction;
import ares.model.board.Tile;
import ares.model.forces.Force;
import ares.model.forces.SurfaceUnit;
import ares.platform.MessageLogger;
import java.util.logging.Logger;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class SurfaceMoveAction extends MoveAction {

    public SurfaceMoveAction(SurfaceUnit unit, ActionType type, Tile origin, Tile destination, int start, Direction fromDir, Scenario scenario) {
        super(unit, type, origin, destination, start, fromDir, scenario);
    }

    public SurfaceMoveAction(SurfaceUnit unit, ActionType type, Tile origin, Tile destination, Direction fromDir, Scenario scenario) {
        this(unit, type, origin, destination, scenario.getClock().getCurrentTime(), fromDir, scenario);
    }

    @Override
    public void execute() {
        Clock clock = scenario.getClock();
        if (checkPreconditions()) {
            Force myForce = unit.getForce();
//            Collection<SurfaceUnit> unitsInDestination = destination.getSurfaceUnits();
            int duration;
            if (!myForce.equals(destination.getOwner()) && destination.getSurfaceUnits().size() > 0) {
                state = ActionState.ABORTED;
                duration = clock.MINUTES_PER_TICK;
                finish = clock.getCurrentTime();
                unit.setOpState(type.getPrecondition());
                
//                System.out.println("[" + clock + "] -> " + "ABORTED " + this.toString());
            } else {
                if (timeToComplete > clock.MINUTES_PER_TICK) {
                    duration = clock.MINUTES_PER_TICK;
                    timeToComplete -= duration;
//                    System.out.println("[" + clock + "] -> " + "ONGOING " + this.toString());
                } else {
                    duration = timeToComplete;
                    timeToComplete = 0;
                    state = ActionState.COMPLETED;
                    finish = clock.getCurrentTime() - clock.MINUTES_PER_TICK + duration;
                    origin.remove(unit);
                    destination.add(unit);
                    unit.setLocation(destination);
                    unit.setOpState(type.getEffectAfter());
//                    System.out.println("[" + clock + "] -> " + "COMPLETED " + this.toString());
                    MessageLogger.info(this.toString());
                }
            }
            int wear = (int) (type.getWearRate() * duration);
            unit.wear(wear);
        } else {
//            System.out.println("[" + clock + "] -> " + "DELAYED " + this.toString());
        }
    }

    @Override
    public String toString() {
        return unit.toString() + " from " + origin + " to " + destination + " at " + (speed * 60.0 / 1000) + " km/h";
    }
//    public String toString() {
//        return unit.toString() + "SurfaceMove #" + id + " at " + (speed * 60.0 / 1000) + " km/h (" + start + "->" + finish + ")";
//    }
}
