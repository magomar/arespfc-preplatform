package ares.engine.command;

import ares.model.board.Tile;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class OperationPlan {

    OperationType type;
    OperationForm form;
    Tile objective;
    
}
