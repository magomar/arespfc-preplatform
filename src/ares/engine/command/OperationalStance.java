package ares.engine.command;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum OperationalStance {
    OFFENSIVE,
    DEFENSIVE,
    RECONNAISSANCE,
    SECURITY,
    RESERVE;
}
