package ares.engine;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum Phase {
    ACT,
    SCHEDULE;
    private Phase next;
    
    static {
        ACT.next = SCHEDULE;
        SCHEDULE.next = ACT;
    }

    public Phase getNext() {
        return next;
    }
    
    
}
