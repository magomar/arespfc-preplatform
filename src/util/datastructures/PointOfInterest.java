package util.datastructures;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public interface PointOfInterest<E> {
    public void next();
    public E getPointOfInterest();
    
}
