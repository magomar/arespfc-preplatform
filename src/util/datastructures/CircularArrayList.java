package util.datastructures;

/**
 * Generic Circular List backed by an array. the list is empty when head == tail. It also means that the elementData
 * array has to have an extra space in it
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("unchecked")
public class CircularArrayList<E> extends AbstractList<E>
        implements List<E>, Serializable {

    private E[] elementData;
    /**
     * points to the first logical element in the array
     */
    private int head = 0;
    /**
     * tail points to the element following the last
     */
    private int tail = 0;
    private int size = 0;

    public CircularArrayList() {
        this(10);
    }

    public CircularArrayList(int size) {
        elementData = (E[]) new Object[size];
    }

    public CircularArrayList(Collection<? extends E> c) {
        tail = c.size();
        elementData = (E[]) new Object[c.size()];
        c.toArray(elementData);
    }

    /**
     * Takes a logical index (as if head was always 0) and calculates the index within elementData
     *
     * @param index
     * @return
     */
    private int convert(int index) {
        return (index + head) % elementData.length;
    }

    @Override
    public boolean isEmpty() {
        return head == tail; // or size == 0
    }

    /**
     * Ensure that the capacity of the list will suffice for the number of elements we want to insert. If it is too
     * small, we make a new, bigger array and copy the old elements in.
     *
     * @param minCapacity
     */
    public void ensureCapacity(int minCapacity) {
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            E[] newData = (E[]) new Object[newCapacity];
            toArray(newData);
            tail = size;
            head = 0;
            elementData = newData;
        }
    }

    @Override
    public int size() {
        // the size can also be worked out each time as:
        // (tail + elementData.length - head) % elementData.length
        return size;
    }

    @Override
    public boolean contains(Object elem) {
        return indexOf(elem) >= 0;
    }

    @Override
    public int indexOf(Object elem) {
        if (elem == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[convert(i)] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elem.equals(elementData[convert(i)])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object elem) {
        if (elem == null) {
            for (int i = size - 1; i >= 0; i--) {
                if (elementData[convert(i)] == null) {
                    return i;
                }
            }
        } else {
            for (int i = size - 1; i >= 0; i--) {
                if (elem.equals(elementData[convert(i)])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    @Override
    public <E> E[] toArray(E[] a) {
        if (a.length < size) {
            a = (E[]) new Object[size];
        }
        if (head < tail) {
            System.arraycopy(elementData, head, a, 0, tail - head);
        } else {
            System.arraycopy(elementData, head, a, 0,
                    elementData.length - head);
            System.arraycopy(elementData, 0, a, elementData.length - head,
                    tail);
        }
        if (a.length > size) {
            a[size] = null;
        }
        return a;
    }

    private void rangeCheck(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException(
                    "Index: " + index + ", Size: " + size);
        }
    }

    @Override
    public E get(int index) {
        rangeCheck(index);
        return (E) elementData[convert(index)];
    }

    @Override
    public E set(int index, E element) {
        modCount++;
        rangeCheck(index);
        E oldValue = elementData[convert(index)];
        elementData[convert(index)] = element;
        return oldValue;
    }

    @Override
    public boolean add(E o) {
        modCount++;
        // We have to have at least one empty space
        ensureCapacity(size + 1 + 1);
        elementData[tail] = o;
        tail = (tail + 1) % elementData.length;
        size++;
        return true;
    }

    /**
     * This method is optimized for removing first and last elements but also allows you to remove in the middle of the
     * list.
     */
    @Override
    public E remove(int index) {
        modCount++;
        rangeCheck(index);
        int pos = convert(index);
        // an interesting application of try/finally is to avoid
        // having to use local variables
        try {
            return (E) elementData[pos];
        } finally {
            elementData[pos] = null; // Let gc do its work
            // optimized for FIFO access, i.e. adding to back and
            // removing from front
            if (pos == head) {
                head = (head + 1) % elementData.length;
            } else if (pos == tail) {
                tail = (tail - 1 + elementData.length) % elementData.length;
            } else {
                if (pos > head && pos > tail) { // tail/head/pos
                    System.arraycopy(elementData, head, elementData, head + 1,
                            pos - head);
                    head = (head + 1) % elementData.length;
                } else {
                    System.arraycopy(elementData, pos + 1, elementData, pos,
                            tail - pos - 1);
                    tail = (tail - 1 + elementData.length) % elementData.length;
                }
            }
            size--;
        }
    }

    public void clear() {
        modCount++;
        // Let gc do its work
        for (int i = head; i != tail; i = (i + 1) % elementData.length) {
            elementData[i] = null;
        }
        head = tail = size = 0;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        modCount++;
        int numNew = c.size();
        // We have to have at least one empty space
        ensureCapacity(size + numNew + 1);
        for (E element : c) {
            elementData[tail] = element;
            tail = (tail + 1) % elementData.length;
            size++;
        }
        return numNew != 0;
    }

    @Override
    public void add(int index, E element) {
        throw new UnsupportedOperationException(
                "Unsupported operation exception");
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException(
                "Unsupported operation exception");
    }

    private synchronized void writeObject(ObjectOutputStream s)
            throws IOException {
        s.writeInt(size);
        for (int i = head; i != tail; i = (i + 1) % elementData.length) {
            s.writeObject(elementData[i]);
        }
    }

    private synchronized void readObject(ObjectInputStream s)
            throws IOException, ClassNotFoundException {
        // Read in size of list and allocate array
        head = 0;
        size = tail = s.readInt();
        elementData = (E[]) new Object[tail];
        // Read in all elements in the proper order.
        for (int i = 0; i < tail; i++) {
            elementData[i] = (E) s.readObject();
        }
    }
}